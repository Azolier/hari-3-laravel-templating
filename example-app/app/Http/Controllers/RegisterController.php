<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form(){
        return view ('form');
    }

    public function sapa(){
        return "OK";
    }

    public function sapa_post(Request $request){
        $pertama = $request->nama1;
        $kedua = $request->nama2;
        // return "Selamat Datang $fname $lname";
        return view('selamat', compact('pertama','kedua'));
    }


}
